# 191103

## SITUATION

The Sahrani Liberation Army (SLA), a once-allied local military faction, has been pushing for more power in the local area through political activism by lobbying with big industrial partners looking to make a quick buck off the oil here.

Normally UAG wouldn't get involved in lower-level local politics like this, but one of our very own intelligence agents has gone AWOL on us and has joined the SLA. We also believe he has found a way to disable implanted "kill-chip".

Due to these facts, and due to the fact that we do not have a substantial garrison present in the area, our marine detachment, "Typhoon", has been called in to take out two birds with one stone: Retrieve the agent, and send a crushing blow to the SLA as a show of force.

## MISSION

Typhoon will be conducting a waterborne insertion under fire and will establish a beachhead at Entrypoint Soggy before moving to neutralise the coastal defences. Once the beachhead is secured, Typhoon will move out on foot to both find the rogue intel agent hiding in a nearby town and neutralise the active defence array protecting the SLA outpost in the area. High command will then call in a strike from ODIN-2, our orbital ordnance delivery satellite, as a show of force and intent to strike down future dissidents.

Mission will be complete upon capture of the rogue agent and destruction of the SLA outpost. The mission will be considered a failure if more than half of BLUFOR is lost.

## EXECUTION

1. Typhoon will insert via and secure the beachhead at Entrypoint Soggy, assisted by fire provided by a destroyer escorting the command ship.
2. Once the beachhead is established, Typhoon will clear the nearby coastal defences.
3. Typhoon will then move to the local village of Everon to retrieve or kill the rogue agent.
4. Finally, Typhoon will disable the SLA outpost's active defence array and oversee bombardment of said outpost before clearing out survivors.

## SERVICE SUPPORT

The command ship is outfitted with infantry rearm facilities.

## COMMAND AND SIGNAL

### Command
Typhoon section command team (TSCT) is ultimately responsible for the success of the mission and is therefor the apex authority on the operation.

### Signal
- **Typhoon:**
    - **Command Net:** Channel 1
    - **Local Net:** Block 1, Channel (1,2,3,4)
